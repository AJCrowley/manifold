import NormalA from 'components/A';

const BrandLink = NormalA.extend`
  height: 100%;
  display: flex;
  align-items: center;
  width: 100%;
  text-decoration: none;
`;

export default BrandLink;
