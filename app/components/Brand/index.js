import React from 'react';
import PropTypes from 'prop-types';

import PullRight from 'components/PullRight';
import BrandLink from './BrandLink';
import Wrapper from './Wrapper';

function Brand(props) {
  const total = Object.keys(props.item.cases).reduce((previous, key) => (previous + props.item.cases[key].amount), 0);
  return (
    <Wrapper>
      <BrandLink href={`brands/${props.item.id}`}>
        {props.item.label} <PullRight>{total}</PullRight>
      </BrandLink>
    </Wrapper>
  );
}

Brand.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Brand;
