import styled from 'styled-components';

const Ul = styled.ul`
  font-size: 1.5em;
  list-style: none;
  margin: 0;
  width: 100%;
  max-height: 30em;
  overflow-y: auto;
  padding: 0 1em;
`;

export default Ul;
