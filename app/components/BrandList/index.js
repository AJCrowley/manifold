import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';

import Brand from 'components/Brand';

import Ul from './Ul';
import Wrapper from './Wrapper';

const BrandList = ({ brands }) => (
  <Wrapper>
    <Ul>
      {map(brands, (brand, key) =>
        <Brand key={`brand-${key}`} item={brand} />
      )}
    </Ul>
  </Wrapper>
);

BrandList.propTypes = {
  brands: PropTypes.object,
};

export default BrandList;
