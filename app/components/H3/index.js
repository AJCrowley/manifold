import styled from 'styled-components';

const H3 = styled.h3`
  color: #b9b9b9;
  font-size: 1em;
  margin: 3em 0 0 0;
  text-transform: uppercase;
`;

export default H3;
