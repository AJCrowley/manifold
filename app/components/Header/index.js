import pint from 'img/pint.png';
import Section from '../Section';

const Header = Section.extend`
  background-color: #f8f8f8;
  background-image: url(${pint});
  background-position: center center;
  background-size: 48px;
  border-bottom: 1px solid #d7d7d7;
  display: block;
  height: 88px;
  left: 0;
  position: absolute;
  text-align: center;
  top: 0;
  width: 100vw;
`;

export default Header;
