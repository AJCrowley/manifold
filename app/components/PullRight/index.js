import Section from '../Section';

const PullRight = Section.extend`
  font-weight: bold;
  height: 3em;
  line-height: 3em;
  margin: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;
`;

export default PullRight;
