import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';

import Router from './router';

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 88px auto 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="Store Manager"
        defaultTitle="Store Manager"
      >
        <meta name="description" content="A pretty great application to manage a store" />
      </Helmet>
      <Router />
    </AppWrapper>
  );
}
