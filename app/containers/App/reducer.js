import { fromJS } from 'immutable';

import {
  LOAD_BRANDS_SUCCESS,
  LOAD_BRANDS,
  LOAD_BRANDS_ERROR,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  brands: {},
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_BRANDS:
      return state
        .set('loading', true)
        .set('error', false)
        .set('brands', {});
    case LOAD_BRANDS_SUCCESS:
      return state
        .set('brands', fromJS(action.brands))
        .set('loading', false);
    case LOAD_BRANDS_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default appReducer;
