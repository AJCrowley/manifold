import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';

import H1 from 'components/H1';
import Header from 'components/Header';

import { load, reset } from './actions';
import { makeSelectLoading, makeSelectBrand, makeSelectCaseAmount } from './selectors';
import saga from './saga';

export class BrandDetailsPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    const { onLoad, match: { params } } = this.props;
    if (params.brandId) onLoad(params.brandId);
  }

  componentWillUnmount() {
    this.props.onReset();
  }

  getCount(type, curCase) {
    return curCase.type === type ? curCase.amount : 0;
  }

  render() {
    const { brand } = this.props;
    const map = new Map();
    if (brand.cases) {
      brand.cases.map((cv) => {
        const newObj = {};
        newObj.size = cv.size;
        newObj[cv.type] = cv.amount;
        return newObj;
      }).sort((a, b) => a.size > b.size ? 1 : -1).forEach((cv) => {
        map.set(cv.size, Object.assign(map.get(cv.size) || {}, cv));
      });
    }
    const cases = Array.from(map.values());
    cases.forEach((cv) => {
      Object.assign(cv, { can: cv.can ? cv.can : 0, bottle: cv.bottle ? cv.bottle : 0 });
    });
    return (
      <div>
        <Helmet>
          <title>{brand.label}</title>
          <meta name="description" content={brand.label} />
        </Helmet>
        <Header />
        &larr; <a href="/">See all brands</a>
        <H1>{brand.label} <p className="casesTotal"><span className="label">Total</span> {this.props.caseAmount}</p></H1>
        <div className="borderBox">
          <table>
            <tbody>
              <tr>
                <th>Case Type</th>
                <th>Bottles</th>
                <th>Cans</th>
              </tr>
              { cases.map((cv) => (
                <tr key={cv.size}>
                  <td>{cv.size} count</td>
                  <td>{cv.bottle}</td>
                  <td>{cv.can}</td>
                </tr>
              )) }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

BrandDetailsPage.propTypes = {
  onLoad: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  brand: PropTypes.object.isRequired,
  caseAmount: PropTypes.number.isRequired,
};

export function mapDispatchToProps(dispatch) {
  return {
    onLoad: (brandId) => dispatch(load(brandId)),
    onReset: () => dispatch(reset()),
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  brand: makeSelectBrand(),
  caseAmount: makeSelectCaseAmount(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'details', saga });

export default compose(
  withSaga,
  withConnect,
)(BrandDetailsPage);
