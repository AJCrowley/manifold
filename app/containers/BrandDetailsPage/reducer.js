import { fromJS } from 'immutable';

import {
  LOAD_SUCCESS,
  LOAD,
  RESET,
} from './constants';

const initialState = fromJS({
  loading: false,
  brand: {},
});

function brandDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD:
      return state.set('loading', true);
    case LOAD_SUCCESS:
      return state.merge({
        loading: false,
        brand: fromJS(action.brand),
      });
    case RESET:
      return initialState;
    default:
      return state;
  }
}

export default brandDetailsReducer;
