import { createSelector } from 'reselect';

const selectBrandDetails = (state) => state.get('brandDetails');

const makeSelectLoading = () => createSelector(
  selectBrandDetails, (brandDetailsState) => brandDetailsState.get('loading')
);

const makeSelectBrand = () => createSelector(
  selectBrandDetails, (brandDetailsState) => brandDetailsState.get('brand').toJS()
);

const makeSelectCaseAmount = () => createSelector(
  makeSelectBrand(), (brand) => {
    let amount = 0;
    if (brand.cases) {
      const caseTypes = brand.cases;
      caseTypes.forEach((caseType) => {
        amount += caseType.amount;
      });
    }
    return amount;
  }
);

export {
  selectBrandDetails,
  makeSelectLoading,
  makeSelectBrand,
  makeSelectCaseAmount,
};
