import React from 'react';
import { Helmet } from 'react-helmet';
import CenteredSection from 'components/CenteredSection';
import H3 from 'components/H3';
import Header from 'components/Header';

export default function NotFound() {
  return (
    <div>
      <Helmet>
        <title>Page Not Found</title>
      </Helmet>
      <Header />
      <CenteredSection>
        <H3>Unable to find page at &quot;{ location.pathname }&quot;</H3>
      </CenteredSection>
      <p>Please click <a href="/">here</a> to return to homepage.</p>
    </div>
  );
}
