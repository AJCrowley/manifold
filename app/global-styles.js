import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fff;
    min-height: 100%;
    min-width: 100%;
  }

  h1, h2, h3, h4, h5, h6 { font-weight: normal; }

  .casesTotal {
    background-color: #ebebeb;
    border-radius: 20px;
    font-size: 0.5em;
    font-weight: bold;
    float: right;
    display: inline-block;
    padding: 0 0.5em;
    -webkit-box-shadow: inset 1px 1px 2px 0px rgba(0,0,0,0.25);
    -moz-box-shadow: inset 1px 1px 2px 0px rgba(0,0,0,0.25);
    box-shadow: inset 1px 1px 2px 0px rgba(0,0,0,0.25);
  }

  .label {
    color: #757575;
    font-size: 0.5em;
    text-transform: uppercase;
    position: relative;
    top: -3px;
  }

  a {
    color: #b9b9b9;
    display: inline-block;
  }

  .borderBox {
    border: 1px #b9b9b9 solid;
    border-radius: 6px;
    padding: 8px;
  }

  table {
    width: 100%;
  }

  th, td {
    padding: 4px;
  }

  th, tr, td {
    border-bottom: 1px #b9b9b9 solid;
  }

  tr:last-child, tr:last-child td {
    border: none;
  }

  th {
    color: #b9b9b9;
    text-align: left;
    text-transform: uppercase;
  }

  th:not(:first-child), td:not(:first-child) {
    text-align: center;
  }

  td:not(:first-child) {
    font-weight: bold;
  }

  p,
  label {
    line-height: 1.5em;
  }
`;
